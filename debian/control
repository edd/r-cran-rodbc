Source: rodbc
Section: gnu-r
Priority: optional
Maintainer: Dirk Eddelbuettel <edd@debian.org>
Build-Depends: debhelper-compat (= 13), dh-r, r-base-dev (>= 4.4.2), unixodbc-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/edd/r-cran-rodbc
Vcs-Git: https://salsa.debian.org/edd/r-cran-rodbc.git
Homepage: https://cran.r-project.org/package=RODBC

Package: r-cran-rodbc
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${R:Depends}
Suggests: odbc-postgresql, libmyodbc
Description: GNU R package for ODBC database access
 This CRAN package provides access to any Open DataBase Connectivity (ODBC)
 accessible database. 
 .
 The package should be platform independent and provide access to any
 database for which a driver exists.  It has been tested with MySQL
 and PostgreSQL on both Linux and Windows (and to those DBMSs on Linux
 hosts from R under Windows), Microsoft Access, SQL Server and Excel
 spreadsheets (read-only), and users have reported success with
 connections to Oracle and DBase.
 .
 Usage is covered in the R Data Import/Export manual (available via the 
 r-doc-pdf, r-doc-html and r-doc-info packages).
